import products from './components/products'
import news from './components/news'

export default [
    ...products,
    ...news
]