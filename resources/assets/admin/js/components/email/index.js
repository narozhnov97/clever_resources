import Table from './table.vue'

export default[
	{
		path: '/email',
        component: Table,
        props: {
            title: 'Рассылка',
            icon: 'mail-reply',
        },    		
	}
]