	@extends('index.layout')

	@section('content')
	<div class="flex-container">
		<div class="title2">
  			{{ $activecategory->name }}
  		</div>
	</div>
	<div class="container list-item">  		
  		@foreach ($products as $product)
  			<a class="inner-a" href="/{{ $product->id }}"> 
		    	<div class="item">
					@if($product->is_by_order)
					<div class="stick2">Под заказ</div>
					@endif
					<div class="img-container">
						<img src="{{$product->image_url}}" alt="">
					</div>
					<p class="name">{{ $product->name }}</p>
					<span class="get-item" href="#" data-popup-open="popup-{{ $product->id }}">Заказать</span>				
				</div>
			</a>
			<div class="popup" data-popup="popup-{{ $product->id }}">
				<div class="popup-inner">
					<h2 class="popUpTitle">Вы заказываете {{ $product->name }}</h2>
					<p class="popUpTitleDescr">После отправления заявки,в ближайшее время, с Вами свяжется наш дизайнер</p>
					<form class="contactUs" role="form" method="post" action="/send">
						<input style="display: none;" name="product" type="text" value="{{ $product->name }}"/>
						<input name="name" type="text" placeholder="Имя" required>
						<input name="email" type="text" placeholder="Email">
						<input name="number" type="tel" placeholder="Телефон" pattern="^(\+375|80)(29|25|44|33)(\d{3})(\d{2})(\d{2})$" required>
						<textarea name="text" placeholder="Cообщение"></textarea>
						<button type="submit">Заказать</button>
					</form>
					<a class="popup-close" data-popup-close="popup-{{ $product->id }}" href="#">x</a>
				</div>
			</div>		
	    @endforeach
	    {{$products->appends(['category_id' => app('request')->input('category_id')])->render()}}	  		
	</div>	
  	<script src="/assets/index/js/jquery-3.2.1.min.js"></script>
  	@endsection