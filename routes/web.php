<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::post('/send', 'EmailController@send');


Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {
    Route::get('/', 'AuthController@login');
    Route::post('/', 'AuthController@doLogin');
    Route::get('/logout', 'AuthController@logout');

    Route::group(['middleware' => 'auth'], function () {
        Route::group(['prefix' => 'api'], function () {
            Route::group(['prefix' => 'products'], function () {
                Route::get('/', 'ProductController@index');
                Route::get('/{product}', 'ProductController@show');
                Route::get('/{product}/gallery', 'ImageController@index');
                Route::post('/', 'ProductController@create');
                Route::post('/{product}', 'ProductController@update');
                Route::post('/{product}/gallery', 'ImageController@create');
                Route::delete('/{product}', 'ProductController@delete');
                Route::delete('/image/{image}', 'ImageController@delete');
            }); 
            Route::group(['prefix' => 'news'], function () {
                Route::get('/', 'NewsController@index');
                Route::get('/{news}', 'NewsController@show');
                Route::post('/', 'NewsController@create');
                Route::post('/{news}', 'NewsController@update');
                Route::delete('/{news}', 'NewsController@delete');
            });       
        });

        Route::any('{all?}', function () {
            return view('admin.main');
        })->where(['all' => '.*']);
    });
});

Route::group(['namespace' => 'Index'], function () {
    Route::get('/', 'HomeController@index');
    Route::get('/catalog', 'CatalogController@index');
    Route::get('/news', 'NewsController@index');
    Route::get('/news/{page}', 'PageController@index');
    Route::get('/{id}', 'InnerController@index');
});

