<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
	protected $fillable = [
        'name',
        'slug',
        'image',
        'description',
        'short_info'
    ];

    protected $appends = ['image_url'];

    public function getImageUrlAttribute()
    {
        return \Storage::disk('public')->url($this->image);
    }    
}
