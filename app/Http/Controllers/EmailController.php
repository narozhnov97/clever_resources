<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class EmailController extends Controller
{

    public function send(Request $request){
        $product_name = $request->input('product', '');
        $title = $product_name ? ('Заказ на ' . $product_name) : 'Новый заказ';
        $name = $request->input('name', '');
        $email = $request->input('email', '');
        $number = $request->input('number', '');
        $text = $request->input('text', '');
        $char = $request->input('char', '');

        Mail::send(
            'emails.order',
            [
                'title' => $title,
                'name' => $name,
                'email' => $email,
                'number' => $number,
                'text' => $text,
                'char' => $char
            ],
            function ($message) use ($title)
            {

                $message->from('cleverbelarus@gmail.com', $title);

                $message->to('cleverbelarus@gmail.com')->subject($title);

            }
        );

        return redirect('/')->with('flash_message', 'Ваша заявка принята');

        // return response()->json(['message' => 'Request completed']);
    }
}
