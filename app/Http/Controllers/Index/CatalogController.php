<?php

namespace App\Http\Controllers\Index;

use App\Category;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Database\Query\Builder;
use App\Http\Controllers\Controller;

class CatalogController extends Controller
{ 

    public function index(Request $request)
	{
		$categories = Category::with('children')->whereNull('parent_id')->get();
		$firstCategory = $categories->first();
		$filters = $request->all();
		$filters['category_id'] = $filters['category_id'] ?? $firstCategory->id;

		$activecategory = Category::where('id', $filters['category_id'])->first();

		$products = Product::filter($filters)->paginate(12);
		return view('index.catalog', compact('products', 'categories', 'filters', 'activecategory'));
	}

}