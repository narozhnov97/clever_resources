<?php

namespace App\Http\Controllers\Index;

use App\Category;
use App\Article;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NewsController extends Controller
{
    public function index()
    {
    	$categories = Category::with('children')->whereNull('parent_id')->get();
		$news = Article::paginate(12);
		return view('index.news', compact('news', 'categories'));
    } 
}