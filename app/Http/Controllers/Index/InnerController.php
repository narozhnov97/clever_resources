<?php

namespace App\Http\Controllers\Index;

use App\Category;
use App\Product;
use App\Characteristic;
use App\Image;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InnerController extends Controller
{
    public function index($id)
    {
    	$categories = Category::with('children')->whereNull('parent_id')->get();
    	$product = Product::where('id', $id)->first();

    	$products = Product::all()->where('category_id', $product->category_id)->take(4);
    	$characteristics = Characteristic::where('category_id', $product->category_id)->get();
    	
        $gallery = Image::where('product_id', $product->id)->get();
    	
		return view('index.inner', compact('categories','product','products', 'characteristics', 'gallery'));
    }
}