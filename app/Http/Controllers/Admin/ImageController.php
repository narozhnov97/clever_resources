<?php

namespace App\Http\Controllers\Admin;

use App\Product;
use App\Image;
use Illuminate\Http\Request;
use Illuminate\Database\Query\Builder;
use App\Http\Controllers\Controller;

class ImageController extends Controller {

	public function index (Product $product) {
		$photos = Image::where('product_id', $product->id)->get();
		return $photos;
	}

	public function create(Product $product, Request $request) {
		Image::create([
            'product_id' => $request->input('product_id'),
            'image' => $request->image->store('products', 'public')
        ]);
		return ['result' => $request];
	}

    public function delete($id)
    {
        $res = Image::where('id', $id)->first();
        \Storage::disk('public')->delete($res->image);
        $res->delete();

        return ['result' => 'success'];
    }	

}