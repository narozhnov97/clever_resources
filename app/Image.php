<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    //
    protected $fillable = [
    	'image',
    	'product_id'
    ];

    protected $appends = ['image_url'];

    public function product() {
    	return $this->belongsTo(Product::class);
    }

	public function getImageUrlAttribute()
    {
        return \Storage::disk('public')->url($this->image);
    }    
}
