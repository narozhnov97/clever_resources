<?php


namespace App;

use App\Category;
use Illuminate\Database\Eloquent\Model;

class Characteristic extends Model
{
	protected $fillable = [
		'name',
		'category_id',
		'options'
	];

	protected $appends = ['options_array'];

	public function category()
    {
        return $this->belongsTo(Category::class);
    }

	public function getOptionsArrayAttribute()
    {
        return explode(',', $this->options);
    }
}